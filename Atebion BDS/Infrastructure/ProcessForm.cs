﻿using System.Configuration;
using System.Net;
using System.Net.Mail;
using Atebion_BDS.Models;

namespace Atebion_BDS.Infrastructure
{
    public class ProcessForm
    {
        public void SendEmail(Contact model)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(AppConfig.EmailRecipient);
            message.To.Add(AppConfig.EmailRecipient);
            message.Subject = "Atebion Contact Submission";
            message.Body = "Name: " + model.FormName + "\nEmail address: " + model.FormEmail + "\nAddress: " + model.FormAddress + "\nSubject: " + model.FormSubject + "\nMessage: " + model.FormMessage;
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            var smtpClient = new SmtpClient(AppConfig.ClientHost,AppConfig.ClientPort)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(AppConfig.HostUsername, AppConfig.HostPassword)
            };

            smtpClient.Send(message.From.ToString(), message.To.ToString(), message.Subject, message.Body);
        }
    }
}
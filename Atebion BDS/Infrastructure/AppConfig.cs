﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Atebion_BDS.Infrastructure
{
    public static class AppConfig
    {
        private static string GetSetting(string settingName)
        {
            return ConfigurationManager.AppSettings[settingName];
        }

        public static int ClientPort
        {
            get { return int.Parse(GetSetting("clientPort")); }
        }

        public static string ClientHost
        {
            get { return GetSetting("clientHost"); }
        }

        public static string HostUsername
        {
            get { return GetSetting("hostUsername"); }
        }

        public static string HostPassword
        {
            get { return GetSetting("hostPassword"); }
        }

        public static string EmailRecipient
        {
            get { return GetSetting("EmailRecipient"); }
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Atebion_BDS.Controller.SurfaceController;
using Atebion_BDS.Models;
using AutoMapper;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Template = Atebion_BDS.Models.Template;

namespace Atebion_BDS.Infrastructure
{
    public class BuildFactory
    {
        private readonly UmbracoHelper _publishedContent;
        private readonly IContentTypeService _contentTypeService;
        public static int RootNodeId = int.Parse(ConfigurationManager.AppSettings["RootNodeId"]);
        private UmbracoBuilder _umbracoBuilder;


        public BuildFactory()
        {
            _publishedContent = new UmbracoHelper(UmbracoContext.Current);
            _contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            _umbracoBuilder = new UmbracoBuilder();
        }

        public HomeModel HomeBuilder()
        {
            return new HomeModel
            {
                CarouselItems = GetCarouselItems()
            };
        }

        public List<CarouselItem> GetCarouselItems()
        {
            var carouselItems = _umbracoBuilder.GetFirstNodeByContentTypeName("Home").Children();
            return carouselItems.Select(carouselItem => new CarouselItem
            {
                Text = _umbracoBuilder.AssignStringValue(carouselItem, "text"),
                PageLink = _umbracoBuilder.GetCarouselLinkById(carouselItem, "pageLink"),
                HideLink = _umbracoBuilder.AssignStringValue(carouselItem, "hideLink"),
                Image = _umbracoBuilder.GetImageMediaId(carouselItem, "image")
            }).ToList();
        }

        public Menu BuildMenu()
        {
            var menuNode = _umbracoBuilder.GetFirstNodeByContentTypeName("Menu");
            return new Menu()
            {
                SubMenus = GetSubMenus(menuNode)
            };
        }

        public List<SubMenu> GetSubMenus(IPublishedContent menuNode)
        {
            var menuItemDocType = _contentTypeService.GetContentType("MenuItem");
            //var templateDocType = _contentTypeService.GetContentType("Template");

            var subMenuList = new List<SubMenu>();

            foreach (var subMenu in menuNode.Children)
            {
                if (subMenu.ContentType.Alias == menuItemDocType.Alias)
                {
                    subMenuList.Add(BuildMenuItem(subMenu));
                }
                else
                {
                    subMenuList.Add(GetTemplate(subMenu));
                }
            }
            return subMenuList.ToList();
        }

        public MenuItem BuildMenuItem(IPublishedContent subMenu)
        {
            return new MenuItem()
            {
                Name = subMenu.Name,
                Subgroups = GetSubgroups(subMenu)
            };
        }

        public List<SubGroup> GetSubgroups(IPublishedContent subMenu)
        {
            return subMenu.Children.Select(subGroup => new SubGroup()
            {
                Name = subGroup.Name,
                Templates = GetTemplates(subGroup)
            }).ToList();
        }

        public Template GetTemplate(IPublishedContent templateNode)
        {
            return ReturnTemplateObject(templateNode);
        }

        public List<Template> GetTemplates(IPublishedContent subGroup)
        {
            return subGroup.Children.Select(ReturnTemplateObject).ToList();
        }

        public Template ReturnTemplateObject(IPublishedContent template)
        {

            var accordionTemplateDocType = _contentTypeService.GetContentType("AccordionTemplate");
            var assetTemplateDocType = _contentTypeService.GetContentType("AssetPortfolioTemplate");
            var imageTemplateDocType = _contentTypeService.GetContentType("ImageTemplate");
            var profilesTemplateDocType = _contentTypeService.GetContentType("ProfilesTemplate");
            var standardTemplateDocType = _contentTypeService.GetContentType("StandardTemplate");

            if (template.ContentType.Alias == accordionTemplateDocType.Alias)
            {
                return new AccordianTemplate()
                {
                    Name = template.Name,
                    ShortUrl = _umbracoBuilder.AssignStringValue(template, "umbracoUrlAlias")
                };
            }
            if (template.ContentType.Alias == assetTemplateDocType.Alias)
            {
                return new AssetPortfolioTemplate()
                {
                    Name = template.Name,
                    ShortUrl = _umbracoBuilder.AssignStringValue(template, "umbracoUrlAlias")
                };
            }
            if (template.ContentType.Alias == imageTemplateDocType.Alias)
            {
                return new ImageTemplate()
                {
                    Name = template.Name,
                    ShortUrl = _umbracoBuilder.AssignStringValue(template, "umbracoUrlAlias")
                };

            }
            if (template.ContentType.Alias == profilesTemplateDocType.Alias)
            {
                return new ProfilesTemplate()
                {
                    Name = template.Name,
                    ShortUrl = _umbracoBuilder.AssignStringValue(template, "umbracoUrlAlias")
                };
            }
            if (template.ContentType.Alias == standardTemplateDocType.Alias)
            {
                return new StandardTemplate()
                {
                    Name = template.Name,
                    ShortUrl = _umbracoBuilder.AssignStringValue(template, "umbracoUrlAlias")
                };
            }
            return new Template();
        }

        public StandardTemplate BuildStandardTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new StandardTemplate()
            {
                Name = node.Name,
                MainText = _umbracoBuilder.AssignStringValue(node, "mainText"),
                QuoteBlock = BuildQuoteBlock(node),
                NodeParentName = node.Ancestor("MenuItem").Name,
                ShortUrl = _umbracoBuilder.AssignStringValue(node, "umbracoUrlAlias")
            };
        }

        public QuoteBlock BuildQuoteBlock(IPublishedContent node)
        {
            return new QuoteBlock()
            {
                QuoteImage = _umbracoBuilder.GetImageMediaId(node, "quoteImage"),
                QuoteText = _umbracoBuilder.AssignStringValue(node, "quoteText"),
                QuoteSource = _umbracoBuilder.AssignStringValue(node, "quoteSource")
            };
        }

        public List<AccordianItem> BuildAccordianBlock(IPublishedContent node)
        {
            var accordianItems = node.Children();
             return accordianItems.Select(accordianItem => new AccordianItem
            {
                Title = accordianItem.Name,
                Text = _umbracoBuilder.AssignStringValue(accordianItem, "richtextEditor"),
            }).ToList();
        }


        public AccordianTemplate BuildAccordianTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new AccordianTemplate()
            {
                Name = node.Name,
                MainText = _umbracoBuilder.AssignStringValue(node, "mainText"),
                QuoteBlock = BuildQuoteBlock(node),
                AccordianItems = BuildAccordianBlock(node),
                NodeParentName = node.Ancestor("MenuItem").Name,
                HideQuote = Boolean.Parse(_umbracoBuilder.AssignStringValue(node, "quoteHide"))
            };
        }

        public ProfilesTemplate BuildProfilesTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);
            
            return new ProfilesTemplate()
            {
                Name = node.Name,
                ProfileItems = BuildProfileItems(node),
                NodeParentName = node.Ancestor("MenuItem").Name
            };

        }

        public List<ProfileItem> BuildProfileItems(IPublishedContent node)
        {
            var profileItems = node.Children();
            return profileItems.Select(profileItem => new ProfileItem
            {
                Name = profileItem.Name,
                MainText = _umbracoBuilder.AssignStringValue(profileItem, "profileDetails"),
                Image = _umbracoBuilder.GetImageMediaId(profileItem, "profileImage")
            }).ToList();
        }

        public ImageTemplate BuildImageTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new ImageTemplate()
            {
                Name = node.Name,
                MainText = _umbracoBuilder.AssignStringValue(node, "mainText"),
                RightText = _umbracoBuilder.AssignStringValue(node, "rightText"),
                Image = _umbracoBuilder.GetImageMediaId(node, "image"),
                NodeParentName = node.Ancestor("MenuItem").Name
            };
        }

        public AssetPortfolioTemplate BuildAssetPortfolioTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new AssetPortfolioTemplate()
            {
                Name = node.Name,
                MainText = _umbracoBuilder.AssignStringValue(node, "mainText"),
                AssetItems = BuildAssetItems(node)
            };
        }

        public List<AssetItem> BuildAssetItems(IPublishedContent node)
        {
            var assetItems = node.Children();
            return assetItems.Select(assetItem => new AssetItem
            {
                Name = assetItem.Name,
                Image = _umbracoBuilder.GetImageMediaId(assetItem, "image"),
                //MainText = _umbracoBuilder.AssignStringValue(assetItem, "mainText"),
                ShortUrl = _umbracoBuilder.AssignStringValue(assetItem, "umbracoUrlAlias")
            }).ToList();
        }


        public AssetItem BuildAssetItemTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new AssetItem()
            {
                Name = node.Name,
                MainText = _umbracoBuilder.AssignStringValue(node, "mainText"),
                Image = _umbracoBuilder.GetImageMediaId(node, "image")
            };
        }

        public Contact BuildContactPage(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new Contact()
            {
                GoogleMapsUrl = _umbracoBuilder.AssignStringValue(node, "googleMapsUrl"),
                AtebionAddress = _umbracoBuilder.AssignStringValue(node, "address"),
                AtebionEmail = _umbracoBuilder.AssignStringValue(node, "emailAddress")
            };
        }

        public NewsTemplate BuildNewsTemplate(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);

            return new NewsTemplate()
            {
                Name = node.Name,
                NewsItems = BuildNewsItems(node),
                ShortUrl = _umbracoBuilder.AssignStringValue(node, "umbracoUrlAlias")
            };
        }

        public List<NewsItem> BuildNewsItems(IPublishedContent node)
        {
            var newsItems = node.Children();
            return newsItems.Select(newsItem => new NewsItem
            {
                Name = newsItem.Name,
                Image = _umbracoBuilder.GetImageMediaId(newsItem, "image"),
                MainText = _umbracoBuilder.AssignStringValue(newsItem, "mainText"),
                Author = _umbracoBuilder.AssignStringValue(newsItem, "author"),
                PublishedDate = _umbracoBuilder.CustomDate(node.CreateDate),
                ShortUrl = _umbracoBuilder.AssignStringValue(newsItem, "umbracoUrlAlias"),
            }).ToList();
        }

        public NewsItem BuildNewsItem(int nodeId)
        {
            var node = _publishedContent.TypedContent(nodeId);
            return new NewsItem
            {
                Name = node.Name,
                Image = _umbracoBuilder.GetImageMediaId(node, "image"),
                MainText = _umbracoBuilder.AssignStringValue(node, "mainText"),
                Author = _umbracoBuilder.AssignStringValue(node, "author"),
                PublishedDate = _umbracoBuilder.CustomDate(node.CreateDate),
                ShortUrl = _umbracoBuilder.AssignStringValue(node, "umbracoUrlAlias"),
            };
        }

    }
}
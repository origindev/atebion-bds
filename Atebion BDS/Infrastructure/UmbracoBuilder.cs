﻿using System;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace Atebion_BDS.Infrastructure
{
    public class UmbracoBuilder
    {

        private readonly UmbracoHelper _publishedContent;
        private readonly IContentTypeService _contentTypeService;

        public UmbracoBuilder() 
        {
            _publishedContent = new UmbracoHelper(UmbracoContext.Current);
            _contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
        }

        public string AssignStringValue(IPublishedContent node, string aliasName)
        {
            var resultString = "";
            if (node.GetPropertyValue(aliasName) != null)
            {
                resultString = node.GetPropertyValue(aliasName).ToString();
            }
            return resultString;
        }

        public string GetImageMediaId(IPublishedContent node, string aliasName)
        {
            var mediaId = node.GetPropertyValue(aliasName) != null ? int.Parse(node.GetPropertyValue(aliasName).ToString()) : 0;
            var mediaService = ApplicationContext.Current.Services.MediaService;
            var image = mediaService.GetById(mediaId);
            return image != null ? image.GetValue("umbracoFile").ToString() : "";
        }

        public IPublishedContent GetFirstNodeByContentTypeName(string nodeName)
        {
            var docType = _contentTypeService.GetContentType(nodeName);
            return _publishedContent.TypedContentAtRoot().First(node => node.ContentType.Alias == docType.Alias);
        }

        public string GetCarouselLinkById(IPublishedContent node, string aliasName)
        {
            var pathId = node.GetPropertyValue(aliasName) != null ? int.Parse(node.GetPropertyValue(aliasName).ToString()) : 0;
            if (pathId == 0) return "";
            var publishedContent = new UmbracoHelper(UmbracoContext.Current);
            var contentNode = publishedContent.TypedContent(pathId);
            return AssignStringValue(contentNode, "umbracoUrlAlias");
        }

        public string CustomDate(DateTime createdDate)
        {
            var day = createdDate.ToString("dd");
            var daySuffix = GetDaySuffix(int.Parse(day));
            var month = createdDate.ToString("MMM");
            var year = createdDate.ToString("yyyy");
            return (month + " " + day + daySuffix + ", " + year);

        }

        static string GetDaySuffix(int day)
        {
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return "st";
                case 2:
                case 22:
                    return "nd";
                case 3:
                case 23:
                    return "rd";
                default:
                    return "th";
            }
        }

    }
}
﻿using System.Configuration;
using System.Web.Mvc;
using Atebion_BDS.Infrastructure;
using Atebion_BDS.Models;
using Umbraco.Web;

namespace Atebion_BDS.Controller.SurfaceController
{
    public class MainSurfaceController: Umbraco.Web.Mvc.SurfaceController
    {
        private BuildFactory _factories;
        private UmbracoHelper _publishedContent;
        private ProcessForm _processForm;



        public MainSurfaceController()
        {
            _factories = new BuildFactory();
            _publishedContent = new UmbracoHelper(UmbracoContext.Current);
            _processForm = new ProcessForm();

        }

        [ChildActionOnly]
        public ActionResult RenderHome()
        {
            //var contentService = Services.ContentService;
            //var homeContent = contentService.GetById(CurrentPage.Id);
            var model = _factories.HomeBuilder();
            return PartialView("Home", model);
        }

        [ChildActionOnly]
        public ActionResult RenderStandardTemplate()
        {
            var model = _factories.BuildStandardTemplate(CurrentPage.Id);
            return PartialView("StandardTemplate", model);
        }

        [ChildActionOnly]
        public ActionResult RenderAccordianTemplate()
        {
            var model = _factories.BuildAccordianTemplate(CurrentPage.Id);
            return PartialView("AccordionTemplate", model);
        }

        [ChildActionOnly]
        public ActionResult RenderProfileTemplate()
        {
            var model = _factories.BuildProfilesTemplate(CurrentPage.Id);
            return PartialView("ProfilesTemplate", model);
        }

        [ChildActionOnly]
        public ActionResult RenderImageTemplate()
        {
            var model = _factories.BuildImageTemplate(CurrentPage.Id);
            return PartialView("ImageTemplate", model);
        }

        [ChildActionOnly]
        public ActionResult RenderAssetPortfolioTemplate()
        {
            var model = _factories.BuildAssetPortfolioTemplate(CurrentPage.Id);
            return PartialView("AssetPortfolioTemplate", model);
        }

        [ChildActionOnly]
        public ActionResult RenderAssetItem()
        {
            var model = _factories.BuildAssetItemTemplate(CurrentPage.Id);
            return PartialView("AssetItem", model);
        }

        public ActionResult RenderContact()
        {
            var model = _factories.BuildContactPage(CurrentPage.Id);
            return PartialView("Contact", model);
        }

        public ActionResult HandleContact(Contact model)
        {
            _processForm.SendEmail(model);
            return CurrentUmbracoPage();
        }

        [ChildActionOnly]
        public ActionResult RenderNewsTemplate()
        {
            var model = _factories.BuildNewsTemplate(CurrentPage.Id);
            return PartialView("NewsTemplate", model);
        }

        [ChildActionOnly]
        public ActionResult RenderNewsItem()
        {
            var model = _factories.BuildNewsItem(CurrentPage.Id);
            return PartialView("NewsItem", model);
        }
        
    }
}
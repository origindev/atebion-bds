﻿using System.Collections.Generic;

namespace Atebion_BDS.Models
{
    public class HomeModel
    {
        public List<CarouselItem> CarouselItems { get; set; }
    }

    public class CarouselItem
    {
        public string Text { get; set; }
        public string Image { get; set; }
        public string PageLink { get; set; }
        public string HideLink { get; set; }
    }

    public class Menu
    {
        public List<SubMenu> SubMenus { get; set; }
    }

    public class SubMenu
    {
    }

    public class MenuItem : SubMenu
    {
        public string Name { get; set; }
        public List<SubGroup> Subgroups { get; set; }
    }

    public class SubGroup : MenuItem
    {
        public string Name { get; set; }
        public List<Template> Templates { get; set; }
    }

    public class Template : SubMenu
    {
    }

    public class AssetPortfolioTemplate : Template
    {
        public string Name { get; set; }
        public string MainText { get; set; }
        public List<AssetItem> AssetItems { get; set; }
        public string ShortUrl { get; set; }
    }

    public class ImageTemplate : Template
    {
        public string Name { get; set; }
        public string MainText { get; set; }
        public string RightText { get; set; }
        public string Image { get; set; }
        public string NodeParentName { get; set; }
        public string ShortUrl { get; set; }
    }

    public class NewsTemplate : Template
    {
        public string Name { get; set; }
        public List<NewsItem> NewsItems { get; set; }
        public string ShortUrl { get; set; }
    }

    public class ProfilesTemplate : Template
    {
        public string Name { get; set; }
        public List<ProfileItem> ProfileItems { get; set; }
        public string NodeParentName { get; set; }
        public string ShortUrl { get; set; }
    }

    public class StandardTemplate : Template
    {
        public string Name { get; set; }
        public string MainText { get; set; }
        public QuoteBlock QuoteBlock { get; set; }
        public string NodeParentName { get; set; }
        public string ShortUrl { get; set; }
    }

    public class AccordianTemplate : Template
    {
        public string Name { get; set; }
        public string MainText { get; set; }
        public QuoteBlock QuoteBlock { get; set; }
        public List<AccordianItem> AccordianItems { get; set; }
        public string NodeParentName { get; set; }
        public bool HideQuote { get; set; }
        public string ShortUrl { get; set; }
    }

    public class Contact
    {
        public string FormName { get; set; }
        public string FormEmail { get; set; }
        public string FormAddress { get; set; }
        public string FormSubject { get; set; }
        public string FormMessage { get; set; }
        public string GoogleMapsUrl { get; set; }
        public string AtebionAddress { get; set; }
        public string AtebionEmail { get; set; }
    }

    public class AssetItem
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string MainText { get; set; }
        public string ShortUrl { get; set; }
    }

    public class QuoteBlock
    {
        public string QuoteImage { get; set; }
        public string QuoteText { get; set; }
        public string QuoteSource { get; set; }
    }

    public class AccordianItem
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }

    public class ProfileItem
    {
        public string Image { get; set; }
        public string Name { get; set; }
        public string MainText { get; set; }
    }

    public class NewsItem
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string MainText { get; set; }
        public string Author { get; set; }
        public string PublishedDate { get; set; }
        public string ShortUrl { get; set; }
    }
}